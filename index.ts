import {chromium} from 'playwright';

(async () => {
  const browser = await chromium.launch();
  const page = await browser.newPage();
  await page.goto('https://k-hirano.com/');
  await page.pdf({path: 'k-hirano.pdf'});
  await browser.close();
})();
